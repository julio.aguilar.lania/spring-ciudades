package mx.lania.ejemplo.ciudades.oad;

import java.util.List;
import mx.lania.ejemplo.ciudades.entidades.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


/**
 *
 * @author jaguilar
 */
public interface OadCountries extends JpaRepository<Country, String>{

    public List<Country> findByNameContainingIgnoreCase(String nombre);
    
    @Query(value="SELECT * FROM countries WHERE surfacearea > 1000000", nativeQuery=true)
    public List<Country> buscarPaisesGrandes(String cadena);
    
}
