package mx.lania.ejemplo.ciudades.control;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
/**
 *
 * @author Jaguilar
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ControladorCatalogosTest {
    
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    /**
     * Test of getPaises method, of class ControladorCatalogos.
     */
    @Test
    public void testGetPaises() {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/countries", String.class))
                .startsWith("[{")
                .endsWith("}]")
                .contains("Afghanistan")
                .contains("Antigua and Barbuda");
    }

    /**
     * Test of getCiudadPorId method, of class ControladorCatalogos.
     */
    //@Test
    public void testGetCiudadPorId() {
    }

    /**
     * Test of getCiudadesPorNombre method, of class ControladorCatalogos.
     */
    @Test
    public void testGetCiudadesPorNombre() {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/countries?name=Alba", String.class))
        .startsWith("[{")
        .endsWith("}]")
        .contains("Albania");
    }

    @Test
    public void testGetCiudadesPorNombreSinResultados() {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/countries?name=XYZ", String.class))
        .isEqualTo("[]");
    }

    /**
     * Test of getPaisesPorNombre method, of class ControladorCatalogos.
     */
//    @Test
    public void testGetPaisesPorNombre() {
    }

    /**
     * Test of crearPais method, of class ControladorCatalogos.
     */
//    @Test
    public void testCrearPais() {
    }

    /**
     * Test of guardarPais method, of class ControladorCatalogos.
     */
//    @Test
    public void testGuardarPais() {
    }
    
}
