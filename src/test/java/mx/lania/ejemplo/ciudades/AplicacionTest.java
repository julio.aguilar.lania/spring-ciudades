package mx.lania.ejemplo.ciudades;

import mx.lania.ejemplo.ciudades.control.ControladorCatalogos;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

/**
 *
 * @author Jaguilar
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AplicacionTest {
    
    /**
     * Test of main method, of class Aplicacion.
     */
    @Test
    public void testMain() {
    }

    @Autowired
    ControladorCatalogos controlador;
    
    @Test
    public void testCreacionControlador() {
        assertThat(controlador)
                .isNotNull();
    }
}
